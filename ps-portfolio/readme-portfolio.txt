===  Pranon Studio Portfolio Plugin ===

Contributors: Pranon Group

Pranon Studio Portfolio Plugin helps to manage your project portfolio showcase.  Supported by Bootstrap and implemented with latest Font Awesome library. It is easy to use.

== Description ==

** Pranon Studio Portfolio Plugin **

= Pranon Studio Portfolio Plugin is one of the easiest plugins to use, even if you are new in WordPress. 

= Just write titles , details, client iformation,total working hour, total personnel for the project and add catagories and tags.

Install plugin and see the result of your creativeness.

= Key Features =

* **Responsive Design ** - Plugin is fully responsive. With responsiveness, everyone will have access to the nicely designed everywhere and every time. If you click on any particular post you will see a single page showing details about that service post.

* **Advanced Compatibility** - Plugin is compatible with Pranon Group plugins and with most of other plugins.

* **Ability to insert a project portfolio to the WordPress post, page** -You can create  as many portfolio posts as you want and show it on your page using shortcode.

* **Shortcode** - Copy and paste the shortcode of the portfolio directly into any WordPress page.add a new page and add shortcode ( write [portfolio] ) in the content of the page

* **Title and Description** - Add unique Title and Description to your post. 

* **Create portfolio in a few minutes** - Add new portfolio or adjust already created the portfolio within a short time.

* **Friendly admin panel** - Easy to use, as the admin panel is user-friendly.

* **portfolio Works in Chrome, Safari, Opera, Firefox** - Open your project portfolio with all the popular browsers.



**Main features of default portfolio **
*Font Awesome icon support.
*Bootstrap Framework based

*Hover Animation

*Add and remove Portfolio item from backend


== Installation ==

### Uploading in WordPress Dashboard

1. First download the ZIP file from Wordpress website
2. Log in to your website administrator panel
3. Go to the 'Add New' in the plugins dashboard, click �Upload Plugin�
4. Upload [Pranon Studio Portfolio ](https://wordpress.org/plugins/ps-portfolio/) ZIP file by choosing it from your computer
5. Click **Install** Now button
6. Then click **Activate Plugin** button.
7. You can see the Portfolio plugin installed on Wordpress left menu.

### Using The WordPress Plugins Dashboard

1. Go to the 'Add New' in the plugins dashboard
2. Search for 'Pranon Studio Portfolio'
3. Click **Install** Now button
4. Then click **Activate Plugin** button
5. You can see the plugin installed on Wordpress left menu

### Using FTP

1. Download the ZIP file from Wordpress website
2. Extract the **ps-portfolio** directory to your computer
3. Upload the ** ps-portfolio** directory to the **/wp-content/plugins/** directory
4. Activate the plugin in the Plugin dashboard
5. You can see the plugin installed on Wordpress left menu


If you think, that you found a bug in our [Pranon Studio Portfolio](https://huge-it.com/ps -portfolio/) plugin or have any question contact us at [info@huge-it.com](mailto:info@huge-it.com)


== Frequently Asked Questions ==

= Do you have some problems with backend? =

*  There can be a few reasons for such problem. In this case, if you have a problem in your portfolio, contact our support to get fast help.
= Where to find functionality details? =

*  All details, including demonstration and description you can find here [portfolio  details,](https://huge-it.com/ps-portfolio /)

= Do you have some problems with installation? =

*  Feel free to ask our support team, they will help you to install the plugin quickly.


= I have purchased the Plugin, and instead of zipping file got it in unzipping form, how to upload in Wordpress? =

*  If you got the file unzip, that means your browser makes auto unzip, please zip the file yourself and upload Pranon Studio Portfolio  into WordPress.


= How to insert portfolio  into Wordpress page or post? =

*  When you create a portfolio in plugin you can go to the post or page editor and there using Add portfolio button insert a portfolio into your WordPress post or page. The other means is, that you can insert your portfolio manually using shortcode.


= How to install plugin? =

*  Go to Plugins page, add the new plugin, upload plugin, and Install Now, Activate Plugin and enjoy the process of creating your portfolio . If you have some questions related with our portfolio plugin feel free to contact our support team we are always ready to help you.


If you think, that you found a bug in our [WordPress Image Portfolio ](https://huge-it.com/ps-portfolio/) plugin or have any question contact us at [info@huge-it.com](mailto:info@huge-it.com).


== Other Notes ==

### Step 1 Creating a portfolio 

**Portfolio -> Add Portfolio **

* Add Images: Set Featured Image from Media Library or upload from computer
* Add title: Give titles
* Add details: Give some information about your project
* Add client name: Add client name of the project
* Add Company Logo: Add image for the client or the company of your project
* Add website: Add the website url of your client
* Add Review: Add the review of your client about your project
* Add another client information: Add as many client information as you want by clicking this button
* Add working hour: Add total working hour for this project
* Add total personnel: Add how many people worked for this project
* Add Taxonomy: Add catagories and tag if needed
* Add Tag: Add tags under a category of a portfolio.


Usage:

* Shortcode: Find shortcode of your portfolio i.e. [portfolio].  Copy and Paste the shortcode in your WordPress page.
