<?php
/**
 * Plugin Name: Pranon Studio Portfolio Plugin
 * Description: A simple description of our plguin
 * Plugin URI: pranonstudio.com
 * Author: Pranon Studio LLC
 * Author URI:pranonstudio.com
 * Version: 0.1
 * License: GPL2
 * Text Domain: ps-portfolio
 */

/**
 * Copyright (c) . All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */

// don't call the file directly

if ( !defined( 'ABSPATH' ) ) {

    exit;
}


// check for required php version and deactivate the plugin if php version is less.
if ( version_compare( PHP_VERSION, '5.4', '<' )) {
    add_action( 'admin_notices', 'ps_notice' );
    function ps_notice() { ?>
        <div class="error notice is-dismissible"> <p>
                <?php
                echo 'This requires minimum PHP 5.4 to function properly. Please upgrade PHP version. The Plugin has been auto-deactivated.. You have PHP version '.PHP_VERSION;
                ?>
            </p></div>
        <?php
        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }

    // deactivate the plugin because required php version is less.
    add_action( 'admin_init', 'ps_deactivate_self' );
    function ps_deactivate_self() {
        deactivate_plugins( plugin_basename( __FILE__ ) );
    }
    return;
}

//----------------------------------------------------------------------
// Core constant defination
//----------------------------------------------------------------------
if (!defined('PS_PORTFOLIO_PLUGIN_VERSION')) define( 'PS_PORTFOLIO_PLUGIN_VERSION', '1.0.0' );
if (!defined('PS_PORTFOLIO_PLUGIN_BASENAME')) define( 'PS_PORTFOLIO_PLUGIN_BASENAME', plugin_basename(__FILE__) );
if (!defined('PS_PORTFOLIO_MINIMUM_WP_VERSION')) define( 'PS_PORTFOLIO_MINIMUM_WP_VERSION', '3.5' );
if (!defined('PS_PORTFOLIO_PLUGIN_DIR')) define( 'PS_PORTFOLIO_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
if (!defined('PS_PORTFOLIO_PLUGIN_URI')) define( 'PS_PORTFOLIO_PLUGIN_URI', plugins_url('', __FILE__) );
if (!defined('PS_PORTFOLIO_PLUGIN_TEXTDOMAIN')) define( 'PS_PORTFOLIO_PLUGIN_TEXTDOMAIN', 'myteam' );

//----------------------------------------------------------------------
// Including Files
//----------------------------------------------------------------------


require_once PS_PORTFOLIO_PLUGIN_DIR . '/include/register-taxonomy.php';
require_once PS_PORTFOLIO_PLUGIN_DIR . '/include/register-post-type.php';
require_once PS_PORTFOLIO_PLUGIN_DIR . '/include/register-metabox.php';
require_once PS_PORTFOLIO_PLUGIN_DIR . '/include/register-tag.php';
require_once PS_PORTFOLIO_PLUGIN_DIR . '/include/portfolio_show.php';
require_once PS_PORTFOLIO_PLUGIN_DIR . '/include/shortcode.php';


//----------------------------------------------------------------------
// Include cmb2 for metabox support
//----------------------------------------------------------------------
if ( file_exists(  __DIR__ . '/admin/cmb2/init.php' ) ) {
    require_once __DIR__ . '/admin/cmb2/init.php';
}
else if
    ( file_exists(  __DIR__ . '/admin/CMB2/init.php' ) ) {
    require_once  __DIR__ . '/admin/CMB2/init.php';
}


//register front end script & style
function ps_portfolio_enqueue_styles(){

//register style sheet

wp_register_style('ps-portfolio-style',PS_PORTFOLIO_PLUGIN_URI .'/css/style.css',true,PS_PORTFOLIO_PLUGIN_VERSION);
wp_register_style('bootstrap',PS_PORTFOLIO_PLUGIN_URI .'/css/bootstrap.min.css',true,PS_PORTFOLIO_PLUGIN_VERSION);
wp_register_style('font-awesome',PS_PORTFOLIO_PLUGIN_URI .'/css/font-awesome.min.css',true,PS_PORTFOLIO_PLUGIN_VERSION);
wp_register_style('ps-portfolio-html-style',PS_PORTFOLIO_PLUGIN_URI .'/css/portfolio.css',true,PS_PORTFOLIO_PLUGIN_VERSION);
wp_register_style('ps-portfolio-pretty-photo-style',PS_PORTFOLIO_PLUGIN_URI .'/css/prettyPhoto.css',true,PS_PORTFOLIO_PLUGIN_VERSION);

//register script

wp_register_script('ps-portfolio-script',PS_PORTFOLIO_PLUGIN_URI .'/js/script.js',array('jquery'),PS_PORTFOLIO_PLUGIN_VERSION,true);
wp_register_script('js-bootstrap',PS_PORTFOLIO_PLUGIN_URI .'/js/bootstrap.min.js',array('jquery'),PS_PORTFOLIO_PLUGIN_VERSION,true);
wp_register_script('js-pretty-photo',PS_PORTFOLIO_PLUGIN_URI .'/js/jquery.prettyPhoto.js',array('jquery'),PS_PORTFOLIO_PLUGIN_VERSION,true);
wp_register_script('jquery',PS_PORTFOLIO_PLUGIN_URI .'/js/jquery-2.1.4.min.js',array('jquery'),PS_PORTFOLIO_PLUGIN_VERSION,true);
//wp_register_script('npm',PS_PORTFOLIO_PLUGIN_URI .'/js/npm.js',array('jquery'),PS_PORTFOLIO_PLUGIN_VERSION,true);
wp_register_script('ps-portfolio-js-style',PS_PORTFOLIO_PLUGIN_URI .'/js/portfolio.js',array('jquery'),PS_PORTFOLIO_PLUGIN_VERSION,true);

//load style sheet
    wp_enqueue_style ('bootstrap');
    wp_enqueue_style ('font-awesome');
    wp_enqueue_style ('ps-portfolio-html-style');
wp_enqueue_style ('ps-portfolio-style');
wp_enqueue_style ('ps-portfolio-pretty-photo-style');

//load script

wp_enqueue_script ('ps-portfolio-script');
wp_enqueue_script ('js-bootstrap');
wp_enqueue_script ('js-pretty-photo');
wp_enqueue_script ('jquery');
//wp_enqueue_script ('npm');
wp_enqueue_script ('ps-portfolio-js-style');
}

//call the function hook

add_action('wp_enqueue_scripts','ps_portfolio_enqueue_styles');

/* Filter the single_template with our custom function*/
//add_filter('single_template', 'my_custom_template');


//register back end script & style

//function ps_portfolio_enque_styles_back(){
//
////register style sheet
//
//wp_register_style('ps_portfolio-style-back',PS_PORTFOLIO_PLUGIN_URI .'admin/css/style.css',true,PS_PORTFOLIO_PLUGIN_VERSION);
//
////register script
//
//wp_register_script('ps_portfolio-script-back',PS_PORTFOLIO_PLUGIN_URI .'admin/js/script.js',array('jquery'),PS_PORTFOLIO_PLUGIN_VERSION,true);
//
////load style sheet
//
//wp_enqueue_style ('ps_portfolio-style-back');
//
////load script
//
//
//wp_enqueue_script ('ps_portfolio-script-back');
//}
//
////call the function hook
//
//add_action('admin_enqueue_scripts','ps_portfolio_enque_styles_back');


