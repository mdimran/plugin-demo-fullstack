<?php

$args = array(
    'post_type'   => 'ps_portfolio_key',
    'post_status' => 'publish',
    'posts_per_page'  => '-1',
);

$query = new WP_Query( $args );

global $post;

?>


<div id='main-wrapper'>
    <div class='content-wrapper'>



        <!-- ps-portfolio-four section start -->
        <section class='ps-portfolio-four padding-top-bottom-100'>

            <div class='portfolio-wraper-four'>
               <!-- <div class='container'> -->
                    <div class='row'>
                        <?php

                            // The Loop
                            if ( $query->have_posts() ) {
                            while ( $query->have_posts() ) { $query->the_post();
                        ?>
                    

                        <div class='col-md-4 nospace'>

                            <div class='portfolio-thumb'>
                                <img class='img-responsive' src='<?php echo get_the_post_thumbnail_url() ?>' alt='<?php the_title() ?>'>

                                <div class='portfolio-content'>
                                    <div class='portfolio-info'>
                                        <h3><?php the_title()?></h3>
                                        <span class='small'><?php echo get_the_term_list($post->ID,'ps_portfolio_tax','<ul class="styles nounderline"><li>', ',</li><li>', '</li></ul>')?></span>

                                    </div> <!-- portfolio-info -->

                                    <div class='pop-up-icon'>
                                        <a class='content-link' href='<?php echo get_post_permalink();?>'><i class='fa fa-link'></i></a>
                                        <a class='img-link'  data-gal='prettyPhoto[pp_gal]' href='<?php echo get_the_post_thumbnail_url() ?>'><i class='fa fa-search'></i></a>



                                    </div> <!-- pop-up-icon -->
                                </div> <!-- portfolio-content -->

                            </div><!-- /.portfolio-thumb -->
                        </div>


                            <?php }

                                wp_reset_postdata();
                            } else {
                                echo "No team Member Found";
                            }
                            ?>

                    </div> <!-- portfolio-wraper-nineteen -->
                <!--</div> -->
            </div>
        </section>
        <!-- ps-portfolio-four section end -->


    </div> <!-- /.content-wrapper -->
</div><!-- /#main-wrapper -->




