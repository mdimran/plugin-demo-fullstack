
<?php

//register front end script & style
function ps_portfolio_single_styles(){
    //register style sheet
    wp_register_style('ps-portfolio-single-style',PS_PORTFOLIO_PLUGIN_URI .'/css/single-page-style.css',true,PS_PORTFOLIO_PLUGIN_VERSION);

    //load style sheet
    wp_enqueue_style ('ps-portfolio-single-style');
}

//call the function hook
add_action('wp_enqueue_scripts','ps_portfolio_single_styles');

function single_ps_portfolio($template) {
    global $post;

// Is this a "my-custom-post-type" post?
    if ($post->post_type == "ps_portfolio_key"){

//Your plugin path
        $plugin_path = plugin_dir_path( __FILE__ );

// The name of custom post type single template
        $template_name = 'single-ps_portfolio_key.php';

// A specific single template for my custom post type exists in theme folder? Or it also doesn't exist in my plugin?
        if($template === get_stylesheet_directory() . '/' . $template_name
            || !file_exists($plugin_path . $template_name)) {

//Then return "single.php" or "single-my-custom-post-type.php" from theme directory.
            return $template;
        }

// If not, return my plugin custom post type template.
        return $plugin_path . $template_name;
    }

//This is not my custom post type, do nothing with $template
    return $template;
}

add_filter('single_template', 'single_ps_portfolio');