<?php

if ( ! function_exists( 'ps_portfolio_tag_func' ) ) {

// Register Custom Taxonomy
function ps_portfolio_tag_func() {

$labels = array(
                    'name'                       => _x( 'Portfolio Tags', 'Taxonomy General Name', 'ps-portfolio' ),
                    'singular_name'              => _x( 'Portfolio Tag', 'Taxonomy Singular Name', 'ps-portfolio' ),
                    'menu_name'                  => __( 'Portfolio Tag', 'ps-portfolio' ),
                    'all_items'                  => __( 'All Tags', 'ps-portfolio' ),
                    'parent_item'                => __( 'Parent Tag', 'ps-portfolio' ),
                    'parent_item_colon'          => __( 'Parent Tag:', 'ps-portfolio' ),
                    'new_item_name'              => __( 'New Tag Name', 'ps-portfolio' ),
                    'add_new_item'               => __( 'Add New Tag', 'ps-portfolio' ),
                    'edit_item'                  => __( 'Edit Tag', 'ps-portfolio' ),
                    'update_item'                => __( 'Update Tag', 'ps-portfolio' ),
                    'view_item'                  => __( 'View Tag', 'ps-portfolio' ),
                    'separate_items_with_commas' => __( 'Separate items with commas', 'ps-portfolio' ),
                    'add_or_remove_items'        => __( 'Add or remove items', 'ps-portfolio' ),
                    'choose_from_most_used'      => __( 'Choose from the most used', 'ps-portfolio' ),
                    'popular_items'              => __( 'Popular Items', 'ps-portfolio' ),
                    'search_items'               => __( 'Search Items', 'ps-portfolio' ),
                    'not_found'                  => __( 'Not Found', 'ps-portfolio' ),
                    'no_terms'                   => __( 'No items', 'ps-portfolio' ),
                    'items_list'                 => __( 'Items list', 'ps-portfolio' ),
                    'items_list_navigation'      => __( 'Items list navigation', 'ps-portfolio' ),
                );
$args = array(
                'labels'                     => $labels,
                'hierarchical'               => false,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => true,
            );

register_taxonomy( 'ps_portfolio_tag', array( 'ps_portfolio_key' ), $args );

}

add_action( 'init', 'ps_portfolio_tag_func', 0 );

}