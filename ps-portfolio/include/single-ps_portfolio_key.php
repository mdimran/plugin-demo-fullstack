<!-- This one goes in plugin folder as MARKUP -->
<?php

get_header(); ?>

<?php

$args = array(
    'post_type' => 'ps_portfolio_key',
    'post_status' => 'publish',
    'posts_per_page' => '-1',
);

$query = new WP_Query($args);

global $post;


?>

<div id="single-main" class="single-project-area section-padding">

    <div class="row">
        <div class="col-md-8">
            <div class="single-project-image">
                <img class='img-responsive project-img' src='<?php echo get_the_post_thumbnail_url() ?>' alt='<?php the_title() ?>'>
            </div>
            <!-- /end single-project-image -->
            <div class="project-content">
                <h3><?php the_title() ?></h3>
                <p>
                    <!--meta data show-->
                    <?php

                    if (!empty(get_post_meta($post->ID, '_portfolio_prefix_details', true))){ ?>
                <p class="#">
                    <?php echo get_post_meta($post->ID, '_portfolio_prefix_details', true) ?>
                </p>

                <?php
                }
                ?>
                </p>
            </div>
            <!-- /end project-content -->
        </div>
        <div class="col-md-4">
            <div class="project-summary">
                <div class="about-project">
                    <span> <?php
                        if(!empty(get_post_meta($post->ID, '_portfolio_prefix_project_time',true))){ ?>
                            <h4>Total Working Hour:
                        <?php } ?>
                            <!--meta data show-->
                            <?php

                            if (!empty(get_post_meta($post->ID, '_portfolio_prefix_project_time', true))) { ?>

                                <?php echo get_post_meta($post->ID, '_portfolio_prefix_project_time', true) ?>


                                <?php
                            }
                            ?>
                        </h4>
                    </span>
                    <!--                        --><?php
                    //                        var_dump(get_post_meta( $post->ID, '_portfolio_prefix_project_time',true));
                    //                        ?>

                    <span>
                        <?php
                        if(!empty(get_post_meta($post->ID, '_portfolio_prefix_person',true))){ ?>
                        <h4>Total Person Worked :
                            <?php } ?>
                            <!--meta data show-->
                            <?php

                            if (!empty(get_post_meta($post->ID, '_portfolio_prefix_person', true))) { ?>

                                <?php echo get_post_meta($post->ID, '_portfolio_prefix_person', true) ?>


                                <?php
                            }
                            ?>
<!--                            --><?php
//                            var_dump(get_post_meta($post->ID, '_portfolio_prefix_project_person', true));
//                            ?>
                        </h4>
                    </span>

                </div>
                <ul class="project-details">
                    <?php
                        if(!empty(get_post_meta($post->ID, '_portfolio_prefix_group',true))){ ?>

                    <h4>Client information</h4>

                        <?php } ?>

                    <ul>
                        <li>
                            <?php
                            $entries = get_post_meta( get_the_ID(), '_portfolio_prefix_group', true );
                            foreach ( (array) $entries as $key => $entry ) {
                                $portfolio_prefix_name=$_portfolio_prefix_image_id=$_portfolio_prefix_website=$_portfolio_prefix_review='';

                                if (isset($entry['_portfolio_prefix_name'])) { ?>
                        <li> <i class="fa fa-user"></i>
                        <strong>Client</strong>: <?php echo esc_html($entry['_portfolio_prefix_name']); ?></li>
                                    <?php
                                }
                                if ( isset( $entry['_portfolio_prefix_image'] ) ) { ?>
                                   <li>
                                    <span class="img-logo"><?php echo wp_get_attachment_image( $entry['_portfolio_prefix_image_id'], 'share-pick', null, array(
                                            'class' => 'thumbnail',
                                           ) ); ?></span></li>
                                    <?php
                                }

                                if (isset($entry['_portfolio_prefix_website'])) { ?>
                                    <li><i class="fa fa-connectdevelop"></i>
                                        <strong>Website</strong>: <a target="_blank" href="<?php echo esc_html($entry['_portfolio_prefix_website']);?>"><?php echo esc_html($entry['_portfolio_prefix_website']);?></a></li>
                                    <?php
                                }
                                if (isset($entry['_portfolio_prefix_review'])) { ?>
                                   <li> <i class="fa fa-file-text-o"></i>
                                       <strong>Review</strong>: <?php echo esc_html($entry['_portfolio_prefix_review']); ?></li>
                                    <?php
                                }


                            }

                            ?>
                        </li>
                    </ul>
            </div>
            <!-- /end project-summary -->
        </div>
        <?php previous_post_link();?>
        <?php next_post_link();?>

    </div>
</div>

<?php

get_footer(); ?>

<?php
