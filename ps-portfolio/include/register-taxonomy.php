<?php
if ( ! function_exists( 'ps_portfolio_taxonomy' ) ) {

// Register Custom Taxonomy
    function ps_portfolio_taxonomy() {

        $labels = array(
            'name'                       => _x( 'Portfolio Taxonomies', 'Taxonomy General Name', 'ps-portfolio' ),
            'singular_name'              => _x( 'Portfolio Taxonomy', 'Taxonomy Singular Name', 'ps-portfolio' ),
            'menu_name'                  => __( 'Portfolio Taxonomy', 'ps-portfolio' ),
            'all_items'                  => __( 'All Portfolios', 'ps-portfolio' ),
            'parent_item'                => __( 'Parent Portfolio', 'ps-portfolio' ),
            'parent_item_colon'          => __( 'Parent Portfolio Item :', 'ps-portfolio' ),
            'new_item_name'              => __( 'New Portfolio', 'ps-portfolio' ),
            'add_new_item'               => __( 'Add New Portfolio', 'ps-portfolio' ),
            'edit_item'                  => __( 'Edit Portfolio', 'ps-portfolio' ),
            'update_item'                => __( 'Update Portfolio', 'ps-portfolio' ),
            'view_item'                  => __( 'View Portfolio', 'ps-portfolio' ),
            'separate_items_with_commas' => __( 'Separate items with commas', 'ps-portfolio' ),
            'add_or_remove_items'        => __( 'Add or remove items', 'ps-portfolio' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'ps-portfolio' ),
            'popular_items'              => __( 'Popular Items', 'ps-portfolio' ),
            'search_items'               => __( 'Search Items', 'ps-portfolio' ),
            'not_found'                  => __( 'Not Found', 'ps-portfolio' ),
            'no_terms'                   => __( 'No items', 'ps-portfolio' ),
            'items_list'                 => __( 'Items list', 'ps-portfolio' ),
            'items_list_navigation'      => __( 'Items list navigation', 'ps-portfolio' ),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
        );
        register_taxonomy( 'ps_portfolio_tax', array( 'ps_portfolio_key' ), $args );

    }
    add_action( 'init', 'ps_portfolio_taxonomy', 0 );

}