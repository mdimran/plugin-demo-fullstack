<?php
add_action( 'cmb2_admin_init', 'ps_portfolio_metabox' );
function ps_portfolio_metabox(){
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_portfolio_prefix_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Test Metabox', 'cmb2' ),
        'object_types'  => array( 'ps_portfolio_key', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        'cmb_styles' => false, // false to disable the CMB stylesheet
        'closed'     => false, // Keep the metabox closed by default

    ) );

    // Regular text field
    $cmb->add_field( array(
        'name'       => __( 'Details', 'cmb2' ),
        'desc'       => 'Company/Client Details',
        'id'         => $prefix . 'details',
        'type'       => 'text',
    ) );




    $group_id = $cmb->add_field( array(
        'id'                => $prefix.'group',
        'type'              => 'group',
        'description'       => 'Client Information',
        'options'           => array(
//            'group_title'   => 'Review {#}',
            'add_button'    => 'Add Another Client Information',
            'remove_button' => 'Remove Client Information',
            'sortable'      => true
        )
    ) );

    $cmb->add_group_field( $group_id, array(
        'id'            => $prefix.'name',
        'name'          => __('Client Name', 'name'),
        'type'          => 'text',
        'desc'          => 'Client Name.'
    ) );


    $cmb->add_group_field( $group_id, array(
        'id'            => $prefix. 'image',
        'name'          => __('Company Logo', 'logo'),
        'type'          => 'file',
        'desc'          => 'Company logo.',
        'text'    => array(
            'add_upload_file_text' => 'Add Logo' // Change upload button text. Default: "Add or Upload File"
        ),


    ) );

    $cmb->add_group_field( $group_id, array(
        'id'            => $prefix. 'website',
        'name'          => __('Company/Client Website', 'website'),
        'type'          => 'text_url',
        'desc'          => 'Website url.'
    ) );

    $cmb->add_group_field( $group_id, array(
        'id'            => $prefix . 'review',
        'name'          => __('Review', 'review'),
        'type'          => 'textarea',
        'desc'          => 'Type the review here.'
    ) );



    // Regular text field

    $cmb->add_field( array(
        'name'       => __( 'Total working hour for project', 'cmb2' ),
        'desc'       => 'Work for any project total time',
        'id'         => $prefix . 'project_time',
        'type'       => 'text_small',
    ) );

    $cmb->add_field( array(
        'name'       => __( 'Total person worked in that project', 'cmb2' ),
        'desc'       => 'Total working person',
        'id'         => $prefix . 'person',
        'type'       => 'text_small',
    ) );


}