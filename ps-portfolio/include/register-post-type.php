<?php

if ( ! function_exists('ps_portfolio_post_type') ) {

// Register Custom Post Type
function ps_portfolio_post_type() {

    $labels = array(
                    'name'                  => _x( 'portfolios', 'Post Type General Name', 'ps-portfolio' ),
                    'singular_name'         => _x( 'porfolio', 'Post Type Singular Name', 'ps-portfolio' ),
                    'menu_name'             => __( 'Portfolio', 'ps-portfolio' ),
                    'name_admin_bar'        => __( 'Portfolio', 'ps-portfolio' ),
                    'archives'              => __( 'Portfolio Archives', 'ps-portfolio' ),
                    'attributes'            => __( 'Portfolio Attributes', 'ps-portfolio' ),
                    'parent_item_colon'     => __( 'Parent Portfolio:', 'ps-portfolio' ),
                    'all_items'             => __( 'All Portfolio', 'ps-portfolio' ),
                    'add_new_item'          => __( 'Add New Portfolio', 'ps-portfolio' ),
                    'add_new'               => __( 'Add Portfolio', 'ps-portfolio' ),
                    'new_item'              => __( 'New Portfolio', 'ps-portfolio' ),
                    'edit_item'             => __( 'Edit Portfolio', 'ps-portfolio' ),
                    'update_item'           => __( 'Update Portfolio', 'ps-portfolio' ),
                    'view_item'             => __( 'View Portfolio', 'ps-portfolio' ),
                    'view_items'            => __( 'View Portfolios', 'ps-portfolio' ),
                    'search_items'          => __( 'Search Portfolio', 'ps-portfolio' ),
                    'not_found'             => __( 'Not found', 'ps-portfolio' ),
                    'not_found_in_trash'    => __( 'Not found in Trash', 'ps-portfolio' ),
                    'featured_image'        => __( 'Featured Image', 'ps-portfolio' ),
                    'set_featured_image'    => __( 'Set featured image', 'ps-portfolio' ),
                    'remove_featured_image' => __( 'Remove featured image', 'ps-portfolio' ),
                    'use_featured_image'    => __( 'Use as featured image', 'ps-portfolio' ),
                    'insert_into_item'      => __( 'Insert into item', 'ps-portfolio' ),
                    'uploaded_to_this_item' => __( 'Uploaded to this item', 'ps-portfolio' ),
                    'items_list'            => __( 'Items list', 'ps-portfolio' ),
                    'items_list_navigation' => __( 'Items list navigation', 'ps-portfolio' ),
                    'filter_items_list'     => __( 'Filter items list', 'ps-portfolio' ),
                );
    $args = array(
                    'label'                 => __( 'porfolio', 'ps-portfolio' ),
                    'description'           => __( 'portfolio', 'ps-portfolio' ),
                    'labels'                => $labels,
                    'supports'              => array( 'title', 'thumbnail', 'comments', ),
                    'taxonomies'            => array( 'ps_portfolio_tax','ps_portfolio_tag' ),
                    'hierarchical'          => true,
                    'public'                => true,
                    'show_ui'               => true,
                    'show_in_menu'          => true,
                    'menu_position'         => 10,
                    'menu_icon'             => 'dashicons-portfolio',
                    'show_in_admin_bar'     => true,
                    'show_in_nav_menus'     => true,
                    'can_export'            => true,
                    'has_archive'           => true,
                    'exclude_from_search'   => false,
                    'publicly_queryable'    => true,
                    'capability_type'       => 'page',
                    );

    register_post_type( 'ps_portfolio_key', $args );

}

add_action( 'init', 'ps_portfolio_post_type', 0 );

}