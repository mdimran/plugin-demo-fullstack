<?php
/**
 * Plugin Name: Pranon Social Share Icons
 * Description: Display Social Share on Post Archive and Single Post
 * Plugin URI:
 * Author: Pranon Studio
 * Author URI:
 * Version: 1.0.0
 * License: GPL2
 * Text Domain: ps-social-icon
 */

/**
 * Copyright (c) PiXelaar LLC . All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */

if ( !defined('ABSPATH') ) die( 'Sorry! This is not your place!' );


//----------------------------------------------------------------------
// Core constant defination
//----------------------------------------------------------------------
if (!defined('PS_SOCIAL_SHARE_PLUGIN_VERSION')) define( 'PS_SOCIAL_SHARE_PLUGIN_VERSION', '1.0.0' );
if (!defined('PS_SOCIAL_SHARE_PLUGIN_BASENAME')) define( 'PS_SOCIAL_SHARE_PLUGIN_BASENAME', plugin_basename(__FILE__) );
if (!defined('PS_SOCIAL_SHARE_MINIMUM_WP_VERSION')) define( 'PS_SOCIAL_SHARE_MINIMUM_WP_VERSION', '3.5' );
if (!defined('PS_SOCIAL_SHARE_PLUGIN_DIR')) define( 'PS_SOCIAL_SHARE_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
if (!defined('PS_SOCIAL_SHARE_PLUGIN_URI')) define( 'PS_SOCIAL_SHARE_PLUGIN_URI', plugins_url('', __FILE__) );
if (!defined('PS_SOCIAL_SHARE_PLUGIN_TEXTDOMAIN')) define( 'PS_SOCIAL_SHARE_PLUGIN_TEXTDOMAIN', 'ps-social-icon' );

//----------------------------------------------------------------------
// Including Files
//----------------------------------------------------------------------


//add submenu
require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/submenu.php';

//add and register settings
require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/add-register-settings.php';

//add front end styles
require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/enque_frontend_styles_and_scripts.php';



if (get_option('ps-social-share-style-option')=='style1'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-1and2.php';
}
//if (get_option('ps-social-share-style-option')=='style2'){
//    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-1and2.php';
//}
if (get_option('ps-social-share-style-option')=='style3'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-3.php';
}
//if (get_option('ps-social-share-style-option')=='style4'){
//    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-4.php';
//}
if (get_option('ps-social-share-style-option')=='style5'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-5.php';
}
if (get_option('ps-social-share-style-option')=='style6'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-6.php';
}
if (get_option('ps-social-share-style-option')=='style7'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-7.php';
}
//if (get_option('ps-social-share-style-option')=='style8'){
//    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-8.php';
//}
if (get_option('ps-social-share-style-option')=='style9'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-8.php';
}
if (get_option('ps-social-share-style-option')=='style10'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-10.php';
}
//if (get_option('ps-social-share-style-option')=='style11'){
//    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-11.php';
//}
if (get_option('ps-social-share-style-option')=='style12'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-12.php';
}
if (get_option('ps-social-share-style-option')=='style13'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-13.php';
}
if (get_option('ps-social-share-style-option')=='style14'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-14.php';
}
//if (get_option('ps-social-share-style-option')=='style15'){
//    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-15.php';
//}
if (get_option('ps-social-share-style-option')=='style16'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-16.php';
}
//if (get_option('ps-social-share-style-option')=='style17'){
//    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-17.php';
//}
if (get_option('ps-social-share-style-option')=='style18'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-20.php';
}
if (get_option('ps-social-share-style-option')=='style19'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-19.php';
}
if (get_option('ps-social-share-style-option')=='style20'){
    require_once PS_SOCIAL_SHARE_PLUGIN_DIR . '/includes/filter-content-for-style-20.php';
}


function ps_social_admin_enque_script(){
    wp_register_style("ps-social-admin-style", PS_SOCIAL_SHARE_PLUGIN_URI . "/admin/css/admin.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION);
    wp_enqueue_style("ps-social-admin-style");


}
add_action("admin_enqueue_scripts", "ps_social_admin_enque_script");