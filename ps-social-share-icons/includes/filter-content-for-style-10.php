<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/6/2017
 * Time: 2:15 AM
 */

function add_social_share_icons10($content){

    if(get_option('ps-social-share-enable')=='yes'){
        $html = "<div class='ps-social'>
    <ul class='ps-social-nav list-inline'>";

        global $post;

        $url = get_permalink($post->ID);
        $url = esc_url($url);

        if (get_option("ps-social-share-facebook") == 1) {
            $html = $html . "<li><a class='ps-icon-button ps-no-style facebook'  target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url ."'><i class='ps-icon-facebook fa fa-facebook'></i><span></span></a></li>";
        }

        if (get_option("ps-social-share-twitter") == 1) {
            $html = $html . "<li><a class='ps-icon-button ps-no-style twitter' target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url ."'><i class='ps-icon-twitter fa fa-twitter'></i><span></span></a></li>";
        }

        if (get_option("ps-social-share-linkedin") == 1) {
            $html = $html . "<li><a class='ps-icon-button ps-no-style linkedin' target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url ."'><i class='ps-icon-linkedin fa fa-linkedin'></i><span></span></a></li>";
        }

        if (get_option("ps-social-share-google") == 1) {
            $html = $html . "<li><a class='ps-icon-button ps-no-style google' target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url ."'><i class='ps-icon-google fa fa-google'></i><span></span></a></li>";
        }
        if (get_option("ps-social-share-reddit") == 1) {
            $html = $html . "<li><a class='ps-icon-button ps-no-style reddit-alien' target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url ."'><i class='ps-icon-reddit-alien fa fa-reddit-alien'></i><span></span></a></li>";
        }
        $html = $html . "</div>";

        $content = $content . $html;

    }

    return $content;

    ?>

    <?php

}

add_filter("the_content", "add_social_share_icons10");
