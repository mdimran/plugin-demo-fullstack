<?php
function ps_social_share_set_settings()
{
    add_settings_section("ps_social_share_config_section", "", null, "ps-social-share");//id=option grp

    add_settings_field("ps-social-share-enable", "Do you want to Enable Social share button?", "ps_social_share_enable_swtich", "ps-social-share", "ps_social_share_config_section");
//    style option
    add_settings_field("ps-social-share-style-option", "Which style do you want to pick?", "ps_social_share_style_option_switch", "ps-social-share", "ps_social_share_config_section");

    add_settings_field("ps-social-share-facebook", "Do you want to display Facebook share button?", "ps_social_share_facebook_checkbox", "ps-social-share", "ps_social_share_config_section");
    add_settings_field("ps-social-share-twitter", "Do you want to display Twitter share button?", "ps_social_share_twitter_checkbox", "ps-social-share", "ps_social_share_config_section");
    add_settings_field("ps-social-share-linkedin", "Do you want to display LinkedIn share button?", "ps_social_share_linkedin_checkbox", "ps-social-share", "ps_social_share_config_section");
    add_settings_field("ps-social-share-google", "Do you want to display google share button?", "ps_social_share_google_checkbox", "ps-social-share", "ps_social_share_config_section");
    add_settings_field("ps-social-share-reddit", "Do you want to display Reddit share button?", "ps_social_share_reddit_checkbox", "ps-social-share", "ps_social_share_config_section");

    register_setting("ps_social_share_config_section", "ps-social-share-enable");
//style op
    register_setting("ps_social_share_config_section", "ps-social-share-style-option");

    register_setting("ps_social_share_config_section", "ps-social-share-facebook");
    register_setting("ps_social_share_config_section", "ps-social-share-twitter");
    register_setting("ps_social_share_config_section", "ps-social-share-linkedin");
    register_setting("ps_social_share_config_section", "ps-social-share-google");
    register_setting("ps_social_share_config_section", "ps-social-share-reddit");
}


function ps_social_share_enable_swtich(){ ?>
    <div class="ps-social-switch">
<!--        <div class="pxlr-switch">-->
        <input type="radio" name="ps-social-share-enable" id="radio1" value="yes" required <?php if(get_option('ps-social-share-enable') =='yes') echo "checked";?>/>
        <label for="radio1">Yes</label>
        <input type="radio" name="ps-social-share-enable" id="radio2" value="no" <?php if(get_option('ps-social-share-enable') =='no') echo "checked";?> required/>
        <label for="radio2">No</label>
    </div>
<?php }

//ps-social-share-style-option

function ps_social_share_style_option_switch(){ ?>
    <div class="ps-style-select">
        <input id="radio21" type="radio" name="ps-social-share-style-option" value="style1"  class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style1') echo "checked";?>/>
        <label class="ps-style style1-image" for="radio21"></label>

<!--        <input id="radio22" type="radio" name="ps-social-share-style-option" value="style2" class="hidden-radio" required --><?php //if(get_option('ps-social-share-style-option') =='style2') echo "checked";?><!--/>-->
<!--        <label class="ps-style style2-image"for="radio22"></label>-->

        <input id="radio23" type="radio" name="ps-social-share-style-option" value="style3" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style3') echo "checked";?>/>
        <label class="ps-style style3-image"for="radio23"></label>
<!---->
<!--        <input id="radio24" type="radio" name="ps-social-share-style-option" value="style4" class="hidden-radio" required --><?php //if(get_option('ps-social-share-style-option') =='style4') echo "checked";?><!--/>-->
<!--        <label class="ps-style style4-image"for="radio24"></label>-->

        <input id="radio25" type="radio" name="ps-social-share-style-option" value="style5" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style5') echo "checked";?>/>
        <label class="ps-style style5-image"for="radio25"></label>

        <input id="radio26" type="radio" name="ps-social-share-style-option" value="style6" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style6') echo "checked";?>/>
        <label class="ps-style style6-image"for="radio26"></label>

<!--        <input id="radio28" type="radio" name="ps-social-share-style-option" value="style8" class="hidden-radio" required --><?php //if(get_option('ps-social-share-style-option') =='style8') echo "checked";?><!--/>-->
<!--        <label class="ps-style style8-image"for="radio28"></label>-->

<!--        <input id="radio29" type="radio" name="ps-social-share-style-option" value="style9" class="hidden-radio" required --><?php //if(get_option('ps-social-share-style-option') =='style9') echo "checked";?><!--/>-->
<!--        <label class="ps-style style9-image"for="radio29"></label>-->

        <input id="radio210" type="radio" name="ps-social-share-style-option" value="style10" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style10') echo "checked";?>/>
        <label class="ps-style style10-image"for="radio210"></label>

<!--        <input id="radio211" type="radio" name="ps-social-share-style-option" value="style11" class="hidden-radio" required --><?php //if(get_option('ps-social-share-style-option') =='style11') echo "checked";?><!--/>-->
<!--        <label class="ps-style style11-image"for="radio211"></label>-->

        <input id="radio212" type="radio" name="ps-social-share-style-option" value="style12" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style12') echo "checked";?>/>
        <label class="ps-style style12-image"for="radio212"></label>

        <input id="radio213" type="radio" name="ps-social-share-style-option" value="style13" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style13') echo "checked";?>/>
        <label class="ps-style style13-image"for="radio213"></label>

        <input id="radio214" type="radio" name="ps-social-share-style-option" value="style14" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style14') echo "checked";?>/>
        <label class="ps-style style14-image"for="radio214"></label>

<!--        <input id="radio215" type="radio" name="ps-social-share-style-option" value="style15" class="hidden-radio" required --><?php //if(get_option('ps-social-share-style-option') =='style15') echo "checked";?><!--/>-->
<!--        <label class="ps-style style15-image"for="radio215"></label>-->

        <input id="radio216" type="radio" name="ps-social-share-style-option" value="style16" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style16') echo "checked";?>/>
        <label class="ps-style style16-image"for="radio216"></label>

<!--        <input id="radio217" type="radio" name="ps-social-share-style-option" value="style17" class="hidden-radio" required --><?php //if(get_option('ps-social-share-style-option') =='style17') echo "checked";?><!--/>-->
<!--        <label class="ps-style style17-image"for="radio217"></label>-->

<!--        <input id="radio218" type="radio" name="ps-social-share-style-option" value="style18" class="hidden-radio" required --><?php //if(get_option('ps-social-share-style-option') =='style18') echo "checked";?><!--/>-->
<!--        <label class="ps-style style18-image"for="radio218"></label>-->

        <input id="radio220" type="radio" name="ps-social-share-style-option" value="style20" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style20') echo "checked";?>/>
        <label class="ps-style style20-image"for="radio220"></label>

        <input id="radio27" type="radio" name="ps-social-share-style-option" value="style7" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style7') echo "checked";?>/>
        <label class="ps-style style7-image"for="radio27"></label>

        <input id="radio219" type="radio" name="ps-social-share-style-option" value="style19" class="hidden-radio" required <?php if(get_option('ps-social-share-style-option') =='style19') echo "checked";?>/>
        <label class="ps-style style19-image"for="radio219"></label>

    </div>
<?php }

    function ps_social_share_facebook_checkbox()
{
    ?>
    <input type="checkbox" name="ps-social-share-facebook"
           value="1" <?php checked(1, get_option('ps-social-share-facebook'), true); ?> /> Check for Yes
    <?php
}

    function ps_social_share_twitter_checkbox()
    {
        ?>
        <input type="checkbox" name="ps-social-share-twitter"
               value="1" <?php checked(1, get_option('ps-social-share-twitter'), true); ?> /> Check for Yes
        <?php
    }

    function ps_social_share_linkedin_checkbox()
    {
        ?>
        <input type="checkbox" name="ps-social-share-linkedin"
               value="1" <?php checked(1, get_option('ps-social-share-linkedin'), true); ?> /> Check for Yes
        <?php
    }

    function ps_social_share_google_checkbox()
    {
        ?>
        <input type="checkbox" name="ps-social-share-google"
               value="1" <?php checked(1, get_option('ps-social-share-google'), true); ?> /> Check for Yes
        <?php
    }

    function ps_social_share_reddit_checkbox()
    {
        ?>
        <input type="checkbox" name="ps-social-share-reddit"
               value="1" <?php checked(1, get_option('ps-social-share-reddit'), true); ?> /> Check for Yes
        <?php
    }

    function ps_social_share_youtube_checkbox()
    {
        ?>
        <input type="checkbox" name="ps-social-share-youtube"
               value="1" <?php checked(1, get_option('ps-social-share-youtube'), true); ?> /> Check for Yes
        <?php
    }

    function ps_social_share_pinterest_checkbox()
    {
        ?>
        <input type="checkbox" name="ps-social-share-pinterest"
               value="1" <?php checked(1, get_option('ps-social-share-pinterest'), true); ?> /> Check for Yes
        <?php
    }
    function ps_social_share_instagram_checkbox()
    {
        ?>
        <input type="checkbox" name="ps-social-share-instagram"
               value="1" <?php checked(1, get_option('ps-social-share-instagram'), true); ?> /> Check for Yes
        <?php
    }
    function ps_social_share_tumbler_checkbox()
    {
        ?>
        <input type="checkbox" name="ps-social-share-tumbler"
               value="1" <?php checked(1, get_option('ps-social-share-tumbler'), true); ?> /> Check for Yes
        <?php
    }



add_action("admin_init", "ps_social_share_set_settings");
