<?php

function add_social_share_icons19($content){

    if(get_option('ps-social-share-enable')=='yes'){
        $html = "<div class='row'>
    <ul class='ps-social'>";

        global $post;

        $url = get_permalink($post->ID);
        $url = esc_url($url);

        if (get_option("ps-social-share-facebook") == 1) {
            $html = $html . "<li ><a class='ps-no-style list-inline fa fa-facebook' target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url ."'></i><span>Facebook</span></a></li>";
        }

        if (get_option("ps-social-share-twitter") == 1) {
            $html = $html . "<li><a  class='ps-no-style list-inline fa fa-twitter' target='_blank' href='https://twitter.com/share?url=" . $url ."'></i><span>Twitter</span></a></li>";
        }

        if (get_option("ps-social-share-linkedin") == 1) {
            $html = $html . "<li><a  class='ps-no-style list-inline fa fa-linkedin' target='_blank' href='http://www.linkedin.com/shareArticle?url=" . $url ."'><span>Linkedin</span></i></a></li>";
        }

        if (get_option("ps-social-share-google") == 1) {
            $html = $html . "<li><a  class='ps-no-style list-inline fa fa-google' target='_blank' href='https://plus.google.com/share?url=" . $url ."'><span>Google</span></i></a></li>";
        }
        if (get_option("ps-social-share-reddit") == 1) {
            $html = $html . "<li><a  class='ps-no-style list-inline fa fa-reddit-alien' target='_blank' href='http://reddit.com/submit?url=" . $url ."'><span>Reddit</span></i></a></li>";
        }
        $html = $html . "</ul></div>";

        $content = $content . $html;

    }

    return $content;

    ?>

    <?php

}

add_filter("the_content", "add_social_share_icons19");

