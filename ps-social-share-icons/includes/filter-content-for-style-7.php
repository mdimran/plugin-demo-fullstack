<?php

function add_social_share_icons7($content)
{

    if(get_option('ps-social-share-enable')=='yes'){
        $html = "<div class='ps-social pull-left'>
                    <ul>";

        global $post;

        $url = get_permalink($post->ID);
        $url = esc_url($url);

        if (get_option("ps-social-share-facebook") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url . "'>Facebook<i class='fa fa-facebook'></i></a></li>";
        }

        if (get_option("ps-social-share-twitter") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='https://twitter.com/share?url=" . $url . "'>Twitter<i class='fa fa-twitter'></i></a></li>";
        }

        if (get_option("ps-social-share-linkedin") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='http://www.linkedin.com/shareArticle?url=" . $url . "'>Linkedin<i class='fa fa-linkedin'></i></a></li>";
        }

        if (get_option("ps-social-share-google") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='https://plus.google.com/share?url=" . $url . "'>Google<i class='fa fa-google'></i></a></li>";
        }
        if (get_option("ps-social-share-reddit") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='http://reddit.com/submit?url=" . $url . "'>Reddit<i class='fa fa-reddit-alien'></i></a></li>";
        }
        $html = $html . "</ul></div>";

        $content = $content . $html;

    }

    return $content;

    ?>

    <?php

}

add_filter("the_content", "add_social_share_icons7");

