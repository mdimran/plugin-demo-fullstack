<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/6/2017
 * Time: 2:10 AM
 */

function add_social_share_icons5($content){

    if(get_option('ps-social-share-enable')=='yes'){
        $html = "<div class='row'>
    <ul class='ps-social list-inline'>";

        global $post;

        $url = get_permalink($post->ID);
        $url = esc_url($url);

        if (get_option("ps-social-share-facebook") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url ."'><i class='fa fa-facebook ps-social-5'></i></a></li>";
        }

        if (get_option("ps-social-share-twitter") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='https://twitter.com/share?url=" . $url ."'><i class='fa fa-twitter ps-social-5'></i></a></li>";
        }

        if (get_option("ps-social-share-linkedin") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='http://www.linkedin.com/shareArticle?url=" . $url ."'><i class='fa fa-linkedin ps-social-5'></i></a></li>";
        }

        if (get_option("ps-social-share-google") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='https://plus.google.com/share?url=" . $url ."'><i class='fa fa-google ps-social-5'></i></a></li>";
        }
        if (get_option("ps-social-share-reddit") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='http://reddit.com/submit?url=" . $url ."'><i class='fa fa-reddit-alien ps-social-5'></i></a></li>";
        }
        $html = $html . "</ul></div>";

        $content = $content . $html;

    }

    return $content;

    ?>

    <?php

}

add_filter("the_content", "add_social_share_icons5");

