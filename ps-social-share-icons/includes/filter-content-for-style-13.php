<?php

function add_social_share_icons13($content){

    if(get_option('ps-social-share-enable')=='yes'){
        $html = "<div class='ps-top-social'>";

        global $post;

        $url = get_permalink($post->ID);
        $url = esc_url($url);

        if (get_option("ps-social-share-facebook") == 1) {
            $html = $html . "<a class='ps-no-style' target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url . "'><i class='fa fa-facebook'></i></a>";
        }

        if (get_option("ps-social-share-twitter") == 1) {
            $html = $html . "<a class='ps-no-style' target='_blank' href='https://twitter.com/share?url=" . $url . "'><i class='fa fa-twitter'></i></a>";
        }

        if (get_option("ps-social-share-linkedin") == 1) {
            $html = $html . "<a class='ps-no-style' target='_blank' href='http://www.linkedin.com/shareArticle?url=" . $url . "'><i class='fa fa-linkedin'></i></a>";
        }

        if (get_option("ps-social-share-google") == 1) {
            $html = $html . "<a class='ps-no-style' target='_blank' href='https://plus.google.com/share?url=" . $url . "'><i class='fa fa-google'></i></a>";
        }
        if (get_option("ps-social-share-reddit") == 1) {
            $html = $html . "<a class='ps-no-style' target='_blank' href='http://reddit.com/submit?url=" . $url . "'><i class='fa fa-reddit-alien'></i></a>";
        }
        $html = $html . "</div>";

        $content = $content . $html;

    }

    return $content;

    ?>

    <?php

}

add_filter("the_content", "add_social_share_icons13");