<?php

//content er sathe amar social icon er html add kora...dynamically !!

function ps_social_share_style()

{
    wp_register_style("bootstrap", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/bootstrap.min.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION);
    wp_register_style("ps-social-share-style-1", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-1-imran.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION);
    wp_register_style("ps-social-share-style-2", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-2-imran.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION);
    wp_register_style("ps-social-share-style-3", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-3-sinthia.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION);
    wp_register_style("ps-social-share-style-8", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-8-sinthia.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-13", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-13-sinthia.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-16", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-16-tuli.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-17", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-17-tuli.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-20", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-20-sinthia.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );



    wp_register_style("ps-social-share-style-4", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-4-rabbi.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-5", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-5-rabbi.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-6", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-6-rabbi.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-10", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-10-tuli.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-12", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-12-tuli.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-19", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-19-rabbi.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-7", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-7-humayara.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );


    wp_register_style("ps-social-share-style-11", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-11-humayara.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-14", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-14-humayara.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );
    wp_register_style("ps-social-share-style-15", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/icon-15-humayara.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );


    wp_enqueue_style("bootstrap");
    wp_enqueue_script('bootstrap-js', PS_SOCIAL_SHARE_PLUGIN_URI . '/js/bootstrap.min.js',array('bootstrap'), PS_SOCIAL_SHARE_PLUGIN_VERSION , false);
    wp_enqueue_script('ps-social-share-script-1', PS_SOCIAL_SHARE_PLUGIN_URI . '/js/script.js',array('jquery'), PS_SOCIAL_SHARE_PLUGIN_VERSION , false);
    wp_register_style("font-awesome-icon", PS_SOCIAL_SHARE_PLUGIN_URI . "/css/font-awesome.min.css",true,PS_SOCIAL_SHARE_PLUGIN_VERSION );




    if (get_option('ps-social-share-style-option')=='style1'){
        wp_enqueue_style("ps-social-share-style-1");

    }
    elseif(get_option('ps-social-share-style-option')=='style2'){

        wp_enqueue_style("ps-social-share-style-2");
    }

    elseif(get_option('ps-social-share-style-option')=='style3'){

        wp_enqueue_style("ps-social-share-style-3");
    }
    elseif(get_option('ps-social-share-style-option')=='style4'){

        wp_enqueue_style("ps-social-share-style-4");
    }
    elseif(get_option('ps-social-share-style-option')=='style5'){

        wp_enqueue_style("ps-social-share-style-5");
    }

    elseif(get_option('ps-social-share-style-option')=='style6'){

        wp_enqueue_style("ps-social-share-style-6");
    }

    elseif(get_option('ps-social-share-style-option')=='style7'){

        wp_enqueue_style("ps-social-share-style-7");
    }

    elseif(get_option('ps-social-share-style-option')=='style8'){

        wp_enqueue_style("ps-social-share-style-8");
    }
//
//    elseif(get_option('ps-social-share-style-option')=='style9'){
//
//        wp_enqueue_style("ps-social-share-style-9");
//    }

    elseif(get_option('ps-social-share-style-option')=='style10'){

        wp_enqueue_style("ps-social-share-style-10");
    }

    elseif(get_option('ps-social-share-style-option')=='style11'){

        wp_enqueue_style("ps-social-share-style-11");
    }
    elseif(get_option('ps-social-share-style-option')=='style12'){

        wp_enqueue_style("ps-social-share-style-12");
    }
    elseif(get_option('ps-social-share-style-option')=='style13'){

        wp_enqueue_style("ps-social-share-style-13");
    }
    elseif(get_option('ps-social-share-style-option')=='style14'){

        wp_enqueue_style("ps-social-share-style-14");
    }
    elseif(get_option('ps-social-share-style-option')=='style15'){

        wp_enqueue_style("ps-social-share-style-15");
    }
    elseif(get_option('ps-social-share-style-option')=='style16'){

        wp_enqueue_style("ps-social-share-style-16");
    }
    elseif(get_option('ps-social-share-style-option')=='style17'){

        wp_enqueue_style("ps-social-share-style-17");
    }
//    elseif(get_option('ps-social-share-style-option')=='style18'){
//
//        wp_enqueue_style("ps-social-share-style-18");
//    }
    elseif(get_option('ps-social-share-style-option')=='style19'){

        wp_enqueue_style("ps-social-share-style-19");
    }
    elseif(get_option('ps-social-share-style-option')=='style20'){

        wp_enqueue_style("ps-social-share-style-20");
    }

    wp_enqueue_style("font-awesome-icon");
}

add_action('wp_enqueue_scripts', 'ps_social_share_style');