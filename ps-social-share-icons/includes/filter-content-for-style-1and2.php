<?php

function add_social_share_icons($content){

    if(get_option('ps-social-share-enable')=='yes'){

        $html = "<ul class='social-icons icon-circle icon-zoom list-unstyled list-inline'>";

        global $post;

        $url = get_permalink($post->ID);
        $url = esc_url($url);

        if (get_option("ps-social-share-facebook") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url . "'><i class='fa fa-facebook'></i></a></li>";
        }

        if (get_option("ps-social-share-twitter") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='https://twitter.com/share?url=" . $url . "'><i class='fa fa-twitter'></i></a></li>";
        }

        if (get_option("ps-social-share-linkedin") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='http://www.linkedin.com/shareArticle?url=" . $url . "'><i class='fa fa-linkedin'></i></a></li>";
        }

        if (get_option("ps-social-share-google") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='https://plus.google.com/share?url=" . $url . "'><i class='fa fa-google'></i></a></li>";
        }
        if (get_option("ps-social-share-reddit") == 1) {
            $html = $html . "<li><a class='ps-no-style' target='_blank' href='http://reddit.com/submit?url=" . $url . "'><i class='fa fa-reddit-alien'></i></a></li>";
        }
        $html = $html . "</ul>";

        $content = $content . $html;

    }

    return $content;

    ?>

    <?php

}

add_filter("the_content", "add_social_share_icons");

//function ps_load_style_1(){
//    wp_enqueue_style("bootstrap");
//    wp_enqueue_style("ps-social-share-style-1");
//}
//function ps_load_style_2(){
//    wp_enqueue_style("bootstrap");
//    wp_enqueue_style("ps-social-share-style-2");
//}