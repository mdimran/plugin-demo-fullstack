<?php
function ps_social_share_icon_menu_item()
{
    add_submenu_page("options-general.php", "PS Social Share", "PS Social Share", "manage_options", "ps-social", "ps_social_share_page");
}

add_action("admin_menu", "ps_social_share_icon_menu_item");


function ps_social_share_page()
{
    ?>
    <div class="wrap">
        <h1>Social Sharing Options</h1>

        <form method="post" action="options.php">
            <?php
            settings_fields("ps_social_share_config_section");
            do_settings_sections("ps-social-share");
            submit_button();
            ?>
        </form>
    </div>
    <?php
}
